import java.util.Arrays;

public class Animal {
    private String name;
    private Employee ic;
    private Employee[] subordinades;
    private DiaryOfDiseases[] diaryOfDiseases;

    public Animal(String name, Employee ic) {
        this(name, ic, new DiaryOfDiseases[0], new Employee[0]);
    }

    public Animal(String name, Employee ic, DiaryOfDiseases[] diaryOfDiseases, Employee[] subordinades) {
        this.name = name;
        this.ic = ic;
        this.diaryOfDiseases = diaryOfDiseases;
        this.subordinades = subordinades;
    }

    public String getName() {
        return name;
    }

    public Employee getIc() {
        return ic;
    }

    public Employee[] getSubordinades() {
        return subordinades;
    }

    public DiaryOfDiseases[] getDiaryOfDiseases() {
        return diaryOfDiseases;
    }

    public void addDisease(DiaryOfDiseases disease) {
        diaryOfDiseases = Arrays.copyOf(diaryOfDiseases, diaryOfDiseases.length + 1);
        diaryOfDiseases[diaryOfDiseases.length - 1] = disease;
    }
}
