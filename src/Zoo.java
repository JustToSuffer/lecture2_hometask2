import java.util.Arrays;

public class Zoo {
    private String title;
    private Animal[] animals;
    private Employee[] employees;

    public Zoo(String title) {
        this(title, new Animal[0], new Employee[0]);
    }

    public Zoo(String title, Animal[] animals, Employee[] employees) {
        this.title = title;
        this.animals = animals;
        this.employees = employees;
    }


    public String getTitle() {
        return title;
    }

    public Animal[] getAnimals() {
        return animals;
    }

    public Employee[] getEmployees() {
        return employees;
    }

    public void add(Employee employee) {
        employees = Arrays.copyOf(employees, employees.length + 1);
        employees[employees.length - 1] = employee;
    }

    public void add(Animal animal) {
        animals = Arrays.copyOf(animals, animals.length + 1);
        animals[animals.length - 1] = animal;
    }

    public void delete(Animal animal) {
        Animal[] newAnimals = new Animal[animals.length - 1];
        for (int i = 0, j = 0; i < animals.length; i++) {
            if (animals[i] != animal) {
                newAnimals[j++] = animals[i];
            }
        }
        animals = newAnimals;
    }

    public void delete(Employee employee) {
        boolean flag = false;
        for (Animal animal : this.animals) {
            if (animal.getIc() == employee) {
                flag = true;
                break;
            }
        }

        if (!flag) {
            Employee[] newEmployees = new Employee[employees.length - 1];
            for (int i = 0, j = 0; i < employees.length; i++) {
                if (employees[i] != employee) {
                    newEmployees[j++] = employees[i];
                }
            }
            employees = newEmployees;
        } else {
            System.out.println("Removing canceled. This employee is busy.");
        }
    }


}
