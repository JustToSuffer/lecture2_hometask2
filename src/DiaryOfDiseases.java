public class DiaryOfDiseases {
    private String nameOfDisease;
    private boolean status;

    public DiaryOfDiseases(String nameOfDisease, boolean status) {
        this.nameOfDisease = nameOfDisease;
    }

    public String getNameOfDisease() {
        return nameOfDisease;
    }

    public boolean getStatus() {
        return status;
    }
}
