public class Main {
    public static void main(String[] args) {
        Zoo zoo = new Zoo("Zoo1");
        zoo.add(new Employee("New", "Guy"));
        zoo.add(new Animal("Fox", zoo.getEmployees()[0]));
        zoo.getAnimals()[0].addDisease(new DiaryOfDiseases("Virus", true));
        System.out.println(zoo.getAnimals()[0].getIc().getFirstName());
        System.out.println(zoo.getAnimals()[0].getDiaryOfDiseases()[0].getNameOfDisease());
        zoo.delete(zoo.getEmployees()[0]);
        zoo.delete(zoo.getAnimals()[0]);
        zoo.delete(zoo.getEmployees()[0]);
    }
}
